// requires...
const path = require('path');
const fs = require('fs');

// constants...

const dirPath =  path.resolve("./files");

// createFile
function createFile (req, res, next) {
  // Your code to create the file.
    const {filename, content} = req.body;
    if(!filename){
        return res.status(400).json({ message: 'Please specify filename parametr' });
    }
    if(!content){
        return res.status(400).json({ message: 'Please specify content parametr' });
    }

    const extentionFiles = path.extname(filename).slice(1);
    const allowedExtention = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

    if (allowedExtention.includes(extentionFiles)) {
        fs.writeFile(path.join(dirPath, filename), content, (err) => {
            if (err) {
                return res.status(500).json({ message: 'Server error' });
            } else {
                return res.status(200).json({
                    message: 'File created successfully',
                });
            }
        });
    } else {
        return res.status(400).json({
            message: `Thе extention ${extentionFiles} doesn't supported`,
        });
    }

    res.status(200).send({ "message": "File created successfully" });
}

//getFiles
function getFiles (req, res, next) {
  // Your code to get all files.
  fs.readdir(dirPath, (err, files) => {
    if (err) {
        return res.status(500).json({ message: 'Server error' });
    }
    if (files.length === 0) {
        console.log('The folder is empthy');
        return res.status(400).json({ message: 'Client error' });
    }
    console.log('Here a list of files');
    return res.status(200).send({
        "message": "Success",
        "files": files
    });  
});
}
  

//getFile
const getFile = (req, res, next) => {
  // Your code to get all files.
  const { filename } = req.params;
  console.log(filename);
  fs.readFile(path.join(dirPath, filename), 'utf-8', (err, data) => {
      if (err) {
          if (err.code === 'ENOENT') {
              return res.status(400).json({
                  message: `No file with ${filename} filename found`,
              });
          }
          return res.status(500).json({ message: 'Server error' });
      } else {
          fs.stat(path.join(dirPath, filename), (err, stats) => {
              if (err) {
                  return res.status(500).json({ message: 'Server error' });
              } else {
                  return res.status(200).send({
                        "message": "Success",
                        "filename": filename,
                        "content": "1. Create GitLab project. 2. Push the homework. 3. Upload homework details to the Excel file",
                        "extension": "txt",
                        "uploadedDate": "2017-07-22T17:32:28Z"
                  });
              }
          });
      }
  });
}



// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
  /* deleteFile, */
  /* editFile */
}
